import axios from "axios";
import { GET_LEADS, DELETE_LEAD, ADD_LEAD } from "./types";

// Note: Actions are managed here (once actions are called the reducers returns the output)

// GET Request on leads (an array) (will be called inside Leads.js function)
export const getLeads = () => (dispatch) => {
  axios
    .get("/api/leads/")
    .then((res) => {
      dispatch({
        type: GET_LEADS,
        payload: res.data,
      });
    })
    .catch((err) => console.log(err));
};

// DELETE Request on leads (will be called inside Leads.js function)
export const deleteLead = (id) => (dispatch) => {
  axios
    .delete(`/api/leads/${id}/`)
    .then((res) => {
      dispatch({
        type: DELETE_LEAD,
        payload: id,
      });
    })
    .catch((err) => console.log(err));
};

// Create Lead Request (will be called inside Form.js function)
export const addLead = (lead) => (dispatch) => {
  axios
    .post("/api/leads/", lead)
    .then((res) => {
      dispatch({
        type: ADD_LEAD,
        payload: res.data,
      });
    })
    .catch((err) => console.log(err));
};
