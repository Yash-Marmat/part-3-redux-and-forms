// A Reducer (manages actions)

import { GET_LEADS, DELETE_LEAD, ADD_LEAD } from "../actions/types.js";

// initial state (like a default value)
const initialState = {
  something: "some text",
  leads: [],
};

// reudcer
export default function (state = initialState, action) {
  switch (action.type) {
    case GET_LEADS:
      return {
        ...state, // will return all the values present in state/initialState
        leads: action.payload, // will come from the server (django api)
      };
    case DELETE_LEAD:
      return {
        ...state,
        leads: state.leads.filter((lead) => lead.id !== action.payload),
      };
    case ADD_LEAD:
      return {
        ...state,
        leads: [...state.leads, action.payload],
      };
    default:
      return state;
  }
}
