import { combineReducers } from "redux";
import leads from "./leads";

export default combineReducers({
  leads, // the leads reducer
});
