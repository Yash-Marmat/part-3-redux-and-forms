import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { getLeads, deleteLead } from "../../actions/leads";

// NOTE: the exported functions like getLeads, deleteLead (from actions) etc. are being
// accessed here by using this.props

class Leads extends Component {
  static propTypes = {
    leads: PropTypes.array.isRequired, // cannot left leads empty now
    getLeads: PropTypes.func.isRequired,
    deleteLead: PropTypes.func.isRequired,
  };

  // calling the getLeads function
  componentDidMount() {
    this.props.getLeads();
  }

  render() {
    return (
      <Fragment>
        <h2>Leads List</h2>
        <table className="tabel tabel-striped">
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Email</th>
              <th>Message</th>
              <th />
            </tr>
          </thead>
          <tbody>
            {this.props.leads.map((lead) => (
              <tr key={lead.id}>
                <td>{lead.id}</td>
                <td>{lead.name}</td>
                <td>{lead.email}</td>
                <td>{lead.message}</td>
                <td>
                  <button
                    onClick={this.props.deleteLead.bind(this, lead.id)}
                    className="btn btn-danger btn-sm"
                  >
                    Delete
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  leads: state.leads.leads,
  // state.leads means from initial state of leads reducer only go for leads (an empty array)
  // and the leads in the last contains value of action.payload (from case: GET_LEADS)
});

export default connect(mapStateToProps, { getLeads, deleteLead })(Leads);
